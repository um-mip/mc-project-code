# Folder structure
coding-project contains the Igor code to analyze the action potential data, run the Cluster and VBW analyses, and generate the MC data.

supporting code contains the routines to perform the higher level analyses (<, >, <=, >=, =, resampling analyses, etc.).

See comments in the folders for further details. See [DB link] for context/data used.
