// modified 20121210 to clarify derivative cutoff value and units
// also storing derivative cutoff in the wave note, and
// renaming data arrays based on wavename.


#pragma rtGlobals=1		// Use modern global access method.

function getparam(boxtitle,prompttext,defaultvalue)
	string boxtitle, prompttext
	variable defaultvalue
	variable input=defaultvalue
	prompt input, prompttext
	DoPrompt boxtitle, input
	return input  
end

// sets up prompt for two numeric entries, returns keyed stringlist
// DO NOT PUT COLONS IN PROMPTTEXT! 
function/s get2params(boxtitle,prompttext,defaultvalue,prompttext2,defaultvalue2)
	string boxtitle, prompttext, prompttext2
	variable defaultvalue,defaultvalue2
	variable input=defaultvalue, input2=defaultvalue2
	prompt input, prompttext
	prompt input2, prompttext2
	
	DoPrompt boxtitle, input, input2
	string output = prompttext + ":" + num2str(input) + ";" + prompttext2 + ":" + num2str(input2) + ";"
	return output  
end