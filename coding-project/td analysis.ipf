#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

Menu "Macros"
//"Hello From Sample Package", HelloFromTDanalysis()
"Unload TD Analysis", UnloadTDanalysis()
End

Function HelloFromTDanalysis()
DoAlert /T="Sample Package Wants to Say" 0, "Hello!"
End

Function UnloadTDanalysis()
Execute /P /Q /Z "DELETEINCLUDE \"td analysis\""
Execute/P/Q/Z "DELETEINCLUDE \"banalysis v1-0\""
Execute/P/Q/Z "DELETEINCLUDE \"burstanalysis v4-0\""
Execute/P/Q/Z "DELETEINCLUDE \"ClusterMasterV4-1\""
Execute/P/Q/Z "DELETEINCLUDE \"ClusterOutputProcessor-v1-3\""
Execute/P/Q/Z "DELETEINCLUDE \"JP_shuffle v0-1\""
Execute/P/Q/Z "DELETEINCLUDE \"JP_smartConcInt_v0.1\""
Execute/P/Q/Z "DELETEINCLUDE \"JP_smart_conc_v2.2.1\""
Execute/P/Q/Z "DELETEINCLUDE \"graphReplicator v0-1\""
Execute/P/Q/Z "DELETEINCLUDE \"tonys_tools\""
Execute/P/Q/Z "DELETEINCLUDE \"hekaFileDefinitions_V1_3\""
Execute/P/Q/Z "DELETEINCLUDE \"readHEKAfiles_v9_0\""

Execute /P /Q /Z "COMPILEPROCEDURES "// Note the space before final quote
End
