#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.

Menu "Macros"
"Load TD analysis", /Q, LoadTDanalysis()
End

Function LoadTDanalysis()

print "Starting to load td analysis package..."

Execute/P/Q/Z "INSERTINCLUDE \"td analysis\""
Execute/P/Q/Z "INSERTINCLUDE \"tonys_tools\""
Execute/P/Q/Z "INSERTINCLUDE \"banalysis v1-0\""
Execute/P/Q/Z "INSERTINCLUDE \"burstanalysis v4-0\""
Execute/P/Q/Z "INSERTINCLUDE \"ClusterMasterV4-1\""
Execute/P/Q/Z "INSERTINCLUDE \"ClusterOutputProcessor-v1-3\""
Execute/P/Q/Z "INSERTINCLUDE \"JP_shuffle v0-1\""
Execute/P/Q/Z "INSERTINCLUDE \"JP_smartConcInt_v0.1\""
Execute/P/Q/Z "INSERTINCLUDE \"JP_smart_conc_v2.2.1\""
Execute/P/Q/Z "INSERTINCLUDE \"graphReplicator v0-1\""
Execute/P/Q/Z "INSERTINCLUDE \"ap_analysis_v1_5\""
Execute/P/Q/Z "INSERTINCLUDE \"hekaFileDefinitions_V1_3\""
Execute/P/Q/Z "INSERTINCLUDE \"readHEKAfiles_v9_0\""

print "td analysis package loaded. Now compiling..."

Execute/P/Q/Z "COMPILEPROCEDURES "// Note the space before final quote

Execute/P "print \"td analysis package compiled...\""
End
