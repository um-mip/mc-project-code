from utilities import NUM_MC_RUNS
from numpy import mean, median
from random import shuffle
import numpy as np
import math

# returns percentage of func(orig, mc), essentially % mc <, <=, etc. orig
def calc_perc(orig_data, mc_data, functor, func_name):
	n = 0
	for mc_run in mc_data:
		if functor(mc_run, orig_data):
			n += 1
	
	# in cases of <=, >=, or == add 1 to mark inclusion of the original
	numerator = n
	if func_name in ("le", "e", "ge"):
		numerator += 1

	pval = numerator / (len(mc_data) + 1)
	assert pval >= 0 and pval <= 1
	return pval

# returns a pval of whether the two groups are different
# based on http://thenode.biologists.com/user-friendly-p-values/research/
def rand_pval(group1, group2, num_draws=1000, use_median=False):
	orig_mean_diff = float('nan')
	if not use_median:
		orig_mean_diff = mean(group1) - mean(group2)
	else:
		orig_mean_diff = median(group1) - median(group2)

	assert not math.isnan(orig_mean_diff)

	merged_groups = np.concatenate((group1, group2))

	mean_diffs = []
	for i in range(num_draws):
		# redraw groups
		shuffle(merged_groups)
		new_group1 = merged_groups[:len(group1)]
		new_group2 = merged_groups[len(group1):len(merged_groups)]
		
		assert len(new_group1) + len(new_group2) == len(merged_groups)
		assert len(new_group1) == len(group1)
		assert len(new_group2) == len(group2)

		# calculate difference in means of new groups
		if not use_median:
			mean_diffs.append(mean(new_group1) - mean(new_group2))
		else:
			mean_diffs.append(median(new_group1) - median(new_group2))

	# check if orig is extreme
	num_geq = 0
	for i in range(len(mean_diffs)):
		if abs(mean_diffs[i]) >= abs(orig_mean_diff):
			num_geq += 1

	# upper bound thing mentioned in the github link
	# don't want a pval of 0
	if not num_geq:
		num_geq = 0.99

	return num_geq / num_draws