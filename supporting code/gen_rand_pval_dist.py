import fileinput
from pvals import rand_pval
from random import randrange, uniform
import numpy as np
import os
import matplotlib.pyplot as plt
from math import floor, ceil

NUM_ITERS = 100
NUM_ITEMS = 1001

class Range:
	def __init__(self, name_in, min_in, max_in, type_in):
		self.name_ = name_in
		self.min_ = min_in
		self.max_ = max_in
		self.type_ = type_in

def read_ranges():
	ranges = []
	for line in fileinput.input():
		line.rstrip("\n")

		# skip comment
		if line[0] == "#" or not line.strip():
			continue

		name, min_, max_, ty = line.split()
		ranges.append(Range(name, min_, max_, ty))

	return ranges

def gen_pvals(r):
	pvals = []
	
	for i in range(NUM_ITERS):
		print("\r%s" % ("Progress: " + str(int(i * 100 / NUM_ITERS))) + "%", end = '\r')
		# gen random ranges
		rand_vals = []
		for i in range(NUM_ITEMS):
			if r.type_ == "i":
				rand_vals.append(randrange(int(float(r.min_)), int(float(r.max_))))
			else:
				rand_vals.append(uniform(float(r.min_), float(r.max_)))

		pvals.append(rand_pval(np.asarray([rand_vals[0]]), np.asarray(rand_vals[1:]), use_median=True))

	return pvals

def write_to_csv(range_name, pvals):
	# image_dir = "/Users/JP/work/mip_dev/rerun/data/bw histos"
	image_dir = "/Users/JP/Desktop/testing"
	# image_dir = "/home/jonp/Desktop/testing2"
	if not os.path.exists(image_dir):
		os.makedirs(image_dir)

	# n, bins, patches = plt.hist(pvals, bins=range(floor(min(pvals)), ceil(max(pvals)) + 2, 1))
	n, bins, patches = plt.hist(pvals, bins=30)
	csv_name = os.path.join(image_dir, range_name + '.csv')
	csv_data = np.array(list(zip(bins, n)))
	np.savetxt(csv_name, csv_data, delimiter=',')

def main():	
	# read ranges from stdin
	ranges = read_ranges()
	
	# run ranges
	for i, r in enumerate(ranges):
		print("Running " + r.name_ + "!", str(int(i * 100 / 96)) + "%" + " completed")
		pvals = gen_pvals(r)
		write_to_csv(r.name_, pvals)

if __name__ == "__main__":
	main()