import os
import array
import csv
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import re

def make_histogram(image_dir, file_name, data):
	plt.clf()

	if not os.path.exists(image_dir):
		os.makedirs(image_dir)

	# n, bins, patches = plt.hist(data, bins=range(0, 3000, 10), density=1, facecolor='blue', alpha=0.5)
	n, bins, patches = plt.hist(data, bins=range(int(min(data)), int(max(data)) + 2, 1), facecolor='blue', density=True, alpha=0.5)

	plt.xlabel('Number of Bursts')
	plt.ylabel('Frequency')
	plt.title('BN for ' + file_name)

	# plt.savefig(os.path.join(image_dir, file_name + '.png'))
	plt.savefig(os.path.join(image_dir, file_name + '.pdf'))

	csv_name = os.path.join(image_dir, file_name + '.csv')
	csv_data = np.array(list(zip(bins, n)))
	np.savetxt(csv_name, csv_data, delimiter=',')

	return

bws = [36]
top_dir = "/Users/JP/work/mip_dev/data/VBW"
image_dir = "/Users/JP/work/mip_dev/rerun/data/bw histos"

for t_item in os.listdir(top_dir):
	# navigate through each group's folder
	group_dir = os.path.join(top_dir, t_item)
	if os.path.isdir(group_dir):
		print(t_item)

		# navigate through each cell folder
		for c_item in os.listdir(group_dir):
			cell_dir = os.path.join(group_dir, c_item)
			if os.path.isdir(cell_dir):
				# iterate through data for each bw
				for bw_item in os.listdir(cell_dir):
					bw_file = os.path.join(cell_dir, bw_item)

					if os.path.isfile(bw_file) and bw_file.endswith("or.txt"):
						# get bw index from file name
						bw_idx = int(re.findall(r"(?<=_)\d+", bw_item)[0])
						if bw_idx in bws:
							# read in original data
							orig_data = np.loadtxt(open(bw_file, "rb"), skiprows=1)
							print(c_item, orig_data[0])
							
							# get corresponding mc file
							mc_file = bw_file.replace("or.txt", "mc.txt")
							mc_data = np.loadtxt(open(mc_file, "rb"), skiprows=1)
							hist_data = []
							for i in range(1000):
								hist_data.append(mc_data[i][0])
							filename = t_item + " " + c_item
							make_histogram(image_dir, filename, hist_data)

