import os
import sys
import argparse
import utilities as ut
import numpy

def print_summary(name, or_data, mc_data, file, minmax_type, bw=-1):
	if not file:	
		if bw != -1:
			print(name, "summary for bw", bw, ":")
		else:
			print(name, "summary :")
		
		
		print("Orig:")
		print("Min:", numpy.nanmin(or_data), "Max:", numpy.nanmax(or_data), "Avg:", numpy.nanmean(or_data), "25th:", numpy.nanpercentile(or_data, 25), "75th:", numpy.nanpercentile(or_data, 75))
	
		print("MC:")
		print("Min:", numpy.nanmin(mc_data), "Max:", numpy.nanmax(mc_data), "Avg:", numpy.nanmean(mc_data), "25th:", numpy.nanpercentile(mc_data, 25), "75th:", numpy.nanpercentile(mc_data, 75))
		
		print("Combined:")
		comb_data = or_data + mc_data
		print("Min:", numpy.nanmin(comb_data), "Max:", numpy.nanmax(comb_data), "Avg:", numpy.nanmean(comb_data), "25th:", numpy.nanpercentile(comb_data, 25), "75th:", numpy.nanpercentile(comb_data, 75))

	else:
		comb_data = or_data + mc_data
		print(name, numpy.nanmin(comb_data), numpy.nanmax(comb_data), minmax_type)

def parse_vbw(seen_groups, bws, file):
	bws_to_use = [6, 21, 36, 51, 66, 81] if bws else range(ut.MAX_BW_IDX)
	
	for param in range(ut.NUM_VBW_PARAMETERS):
		if not file:	
			print("# **************", ut.get_vbw_output_type(param), "**************")
		
		type_ = "i" if param in (0, 4) else "f"

		global_mc_data = []
		global_or_data = []

		for bw in bws_to_use:
			if not file:
				print("# BW:", bw)
		
			bw_mc_data = []
			bw_or_data = []

			for group in seen_groups:
				if not file:
					print("# " + group.get_name())
		
				group_mc_data = []
				group_or_data = []
				for cell in range(group.get_num_cells()):
					od, mc = group.get_cell(cell).get_bw_data(bw, param)

					bw_or_data.append(od)
					global_or_data.append(od)
					group_or_data.append(od)

					bw_mc_data.extend(mc)
					global_mc_data.extend(mc)
					group_mc_data.extend(mc)

				print_summary(ut.get_vbw_output_type(param) + "_" + group.get_name().replace(" ", "") + "_" + str(bw), group_or_data, group_mc_data, file, type_, bw=bw)
				if not file:
					print("")

			print_summary(ut.get_vbw_output_type(param) + "_" + str(bw), bw_or_data, bw_mc_data, file, type_, bw=bw)
			if not file:
				print("")

		print_summary(ut.get_vbw_output_type(param) + "_global", global_or_data, global_mc_data, file, type_)
		if not file:
			print("")

def parse_cluster(seen_groups, file):
	for param in range(ut.NUM_CLUSTER_PARAMETERS):
		type_ = "i" if param in (2, 3) else "f"

		global_mc_data = []
		global_or_data = []

		if not file:
			print("# **************", ut.get_cl_output_type(param), "**************")

		for group in seen_groups:
			if not file:
				print("# " + group.get_name())
	
			group_mc_data = []
			group_or_data = []
			for cell in range(group.get_num_cells()):
				od, mc = group.get_cell(cell).get_data(param)

				global_or_data.append(od)
				group_or_data.append(od)

				global_mc_data.extend(mc)
				group_mc_data.extend(mc)

			print_summary(ut.get_cl_output_type(param) + "_" + group.get_name().replace(" ", ""), group_or_data, group_mc_data, file, type_)
			if not file:
				print("")	

		print_summary(ut.get_cl_output_type(param) + "_global", global_or_data, global_mc_data, file, type_)
		if not file:
			print("")

def main(bws, file):
	top_dir = "/Users/JP/work/mip_dev/data/"
	
	bw_seen_groups, bw_group_names = ut.read_vbw_data(top_dir + "VBW")
	if not file:
		print("Starting vbw!")
	parse_vbw(bw_seen_groups, bws, file)

	cl_seen_groups, cl_group_names = ut.read_cl_data(top_dir + "Cluster")
	if not file:
		print("Starting cluster!")
	parse_cluster(cl_seen_groups, file)

	if not file:
		print("Finished!")

def process_arguments(args):
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-b', '--bws',
                        action='store_true',
                        default=False,
                        required=False,
                        help='use hardcoded bws instead of everything')
    parser.add_argument('-f', '--file',
                        action='store_true',
                        default=False,
                        required=False,
                        help='write to file that can be used by gen_rand')

    
    return vars(parser.parse_args(args))

if __name__ == "__main__":
	parameters = process_arguments(sys.argv[1:])
	main(**parameters)