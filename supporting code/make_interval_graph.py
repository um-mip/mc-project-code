import os
import array
import csv
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

def make_histogram(image_dir, file_name, data):
	plt.clf()

	if not os.path.exists(image_dir):
		os.makedirs(image_dir)

	# n, bins, patches = plt.hist(data, bins=range(0, 3000, 10), density=1, facecolor='blue', alpha=0.5)
	n, bins, patches = plt.hist(data, bins=range(0, 3000, 10), facecolor='blue', alpha=0.5)

	plt.xlabel('Interval dur. (ms)')
	plt.ylabel('Frequency')
	plt.title('Intervals for ' + file_name)

	# plt.savefig(os.path.join(image_dir, file_name + '.png'))
	plt.savefig(os.path.join(image_dir, file_name + '.pdf'))

	csv_name = os.path.join(image_dir, file_name + '.csv')
	csv_data = np.array(list(zip(bins, n)))
	np.savetxt(csv_name, csv_data, delimiter=',')

	return

all_hist_data = []
top_dir = "/Users/JP/work/mip_dev/data/new_intervals"

# top level folder
for t_item in os.listdir(top_dir):
	group_hist_data = []

	# navigate through each group's folder
	group_dir = os.path.join(top_dir, t_item)
	if (os.path.isdir(group_dir)):
		for c_item in os.listdir(group_dir):
			cell_file = os.path.join(group_dir, c_item)
			if os.path.isfile(cell_file) and cell_file.endswith(".txt"):
				# get data from file
				data = np.loadtxt(open(cell_file, "rb"))
				cell_hist_data = data * 1000

				# make cell histogram
				make_histogram(group_dir, os.path.splitext(c_item)[0], cell_hist_data)

				# add to group data
				for i in cell_hist_data:
					group_hist_data.append(i)

		# make group histogram
		make_histogram(top_dir, os.path.splitext(t_item)[0], group_hist_data)

		# add to massive histogram
		for i in group_hist_data:
			all_hist_data.append(i)

# make big histogram
make_histogram(top_dir, "all_groups", all_hist_data)
