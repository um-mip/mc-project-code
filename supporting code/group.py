import utilities as ut
import numpy
import statistics

class Group:
	def __init__(self, name):
		self.name = name
		self.cells = []

	def get_name(self):
		return self.name

	def get_num_cells(self):
		return len(self.cells)

	def get_cell(self, i):
		return self.cells[i]

	def add_cell(self, cell_to_add):
		self.cells.append(cell_to_add)