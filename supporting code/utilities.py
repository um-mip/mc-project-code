import os
from group import Group
from cell import VBW_Cell, Cluster_Cell
import re
import numpy
import matplotlib.pyplot as plt
import pylab
import csv
from itertools import zip_longest
import statistics
import random

NUM_VBW_PARAMETERS = 9
NUM_CLUSTER_PARAMETERS = 10
MAX_BW_IDX = 201
NUM_MC_RUNS = 1000

def less(lhs, rhs):
	return lhs < rhs

def greater(lhs, rhs):
	return lhs > rhs

def equals(lhs, rhs):
	return lhs == rhs

def greater_eq(lhs, rhs):
	return lhs >= rhs

def less_eq(lhs, rhs):
	return lhs <= rhs

func_names = ['l', 'le', 'e', 'ge', 'g']
functors = [less, less_eq, equals, greater_eq, greater]

def get_vbw_output_type(j):
	if j == 0: 
		return "bn"
	elif j == 1:
		return "mbd"
	elif j == 2:
		return "spb"
	elif j == 3:
		return "bf"
	elif j == 4:
		return "ssn"
	elif j == 5:
		return "ssf"
	elif j == 6:
		return "tf"
	elif j == 7:
		return "inter"
	elif j == 8:
		return "intra"
	else:
		exit(1)

def get_cl_output_type(j):
	if j == 0: 
		return "dur"
	elif j == 1:
		return "freq"
	elif j == 2:
		return "numPeaks"
	elif j == 3:
		return "numNadirs"
	elif j == 4:
		return "meanPeakDur"
	elif j == 5:
		return "totalPeakDur"
	elif j == 6:
		return "meanNadirDur"
	elif j == 7:
		return "totalNadirDur"
	elif j == 8:
		return "meanPeakAmpPeak"
	elif j == 9:
		return "meanNadirAmpPeak"
	else:
		exit(1)

def read_vbw_data(top_dir):
	seen_groups = []
	group_names = []
	print("Reading data from", top_dir, "!")
	# top level folder
	for t_item in os.listdir(top_dir):
		# navigate through each group's folder
		group_dir = os.path.join(top_dir, t_item)
		if os.path.isdir(group_dir):
			# decided is a group, create new one to reflect
			new_group = Group(t_item)
			print(t_item)

			# navigate through each cell folder
			for c_item in os.listdir(group_dir):
				cell_dir = os.path.join(group_dir, c_item)
				if os.path.isdir(cell_dir):
					# decided have new cell, create one to reflect
					new_cell = VBW_Cell(c_item)
					print(c_item)

					# iterate through data for each bw
					for bw_item in os.listdir(cell_dir):
						bw_file = os.path.join(cell_dir, bw_item)

						if os.path.isfile(bw_file) and bw_file.endswith("or.txt"):
							# get bw index from file name
							bw_idx = int(re.findall(r"(?<=_)\d+", bw_item)[0])

							# read in original data
							orig_data = numpy.loadtxt(open(bw_file, "rb"), skiprows=1)
							# get corresponding mc file
							mc_file = bw_file.replace("or.txt", "mc.txt")
							mc_data = numpy.loadtxt(open(mc_file, "rb"), skiprows=1)

							new_cell.add_orig_data(orig_data, bw_idx)
							new_cell.add_mc_data(mc_data, bw_idx)

					# done modifying this cell, add to group
					new_group.add_cell(new_cell)

			# done modifying group, add to list of groups
			seen_groups.append(new_group)
			group_names.append(new_group.get_name())
	print("Finished reading data!")

	return seen_groups, group_names

def read_cl_data(top_dir):
	seen_groups = []
	group_names = []
	print("Reading data from", top_dir, "!")

	# top level folder
	for t_item in os.listdir(top_dir):
		# navigate through each group's folder
		group_dir = os.path.join(top_dir, t_item)
		if os.path.isdir(group_dir):
			# decided is a group, create new one to reflect
			new_group = Group(t_item)
			print(t_item)

			# navigate through each cell folder
			for c_item in os.listdir(group_dir):
				cell_dir = os.path.join(group_dir, c_item)
				if os.path.isdir(cell_dir):
					# decided have new cell, create one to reflect
					new_cell = Cluster_Cell(c_item)
					print(c_item)

					# iterate through data for each cell
					for cl_item in os.listdir(cell_dir):
						cl_file = os.path.join(cell_dir, cl_item)

						if os.path.isfile(cl_file) and cl_file.endswith("or.txt"):
							# read in original data
							orig_data = numpy.loadtxt(open(cl_file, "rb"), skiprows=1)
							# get corresponding mc file
							mc_file = cl_file.replace("or.txt", "cl.txt")
							mc_data = numpy.loadtxt(open(mc_file, "rb"), skiprows=1)

							new_cell.add_orig_data(orig_data)
							new_cell.add_mc_data(mc_data)

					# done modifying this cell, add to group
					new_group.add_cell(new_cell)

			# done modifying group, add to list of groups
			seen_groups.append(new_group)
			group_names.append(new_group.get_name())

	print("Finished reading data!")

	return seen_groups, group_names

def gen_cell_image(img_name, data_to_plot, group_names, a_type, o_type, bw=-1):
	plt.clf()
	fig = plt.figure(1, frameon=False)
	ax = fig.add_subplot(111)
	bp = ax.boxplot(data_to_plot, notch=True, showfliers=False, showmeans=True, bootstrap=5000)

	# show data points
	for z in range(len(group_names)):
		y = data_to_plot[z]
		x = numpy.random.normal(1 + z, 0.025, size=len(y))
		pylab.plot(x, y, 'r.', alpha=0.5)
	
	# add labels, remove misc ticks
	ax.set_xticklabels(group_names)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()
	plt.ylabel("Percentile rank of original value")
	if a_type == "VBW":
		plt.title("BW " + str(bw) + " " + get_vbw_output_type(o_type))
	else:
		plt.title(get_cl_output_type(o_type))
	
	# save figure
	fig.savefig(img_name) #, bbox_inches='tight')

def gen_cell_csv(csv_name, data_to_plot):
	with open(csv_name, "w") as f:
		writer = csv.writer(f)
		writer.writerows(zip(*[["Adult PNA"], ["Adult VEH"], ["P21 PNA"], ["P21 VEH"]]))
		writer.writerows(zip_longest(*data_to_plot))