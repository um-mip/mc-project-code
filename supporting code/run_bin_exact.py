import os
import argparse
import sys
import numpy
from scipy.stats import binom_test

def get_san_input(input_folder):
	data = []
	for item in os.listdir(input_folder):
		file = os.path.join(input_folder, item)

		# read, fills in blanks with nasn
		if os.path.isfile(file) and file.endswith(".csv"):
			raw_data = numpy.genfromtxt(open(file, "rb"), skip_header=1, delimiter=",")

		new_cols = []
		for i in range(len(raw_data[0])):
			new_cols.append([x for x in raw_data[:, i] if not numpy.isnan(x)])

		data.append((item, new_cols))

	return data

def bin_exact(data):
	group_names = ["Adult PNA", "Adult VEH", "P21 PNA", "P21 VEH"]

	results = []
	for pair in data:
		for i, col in enumerate(pair[1]):

			num_sig = sum(1 for j in col if j < 0.05)
			num_elts = len(col)
			prob = 1/float(len(col))
			# prob = 1/10

			print(pair[0], group_names[i%len(group_names)], num_sig, num_elts, prob, sep="\t")

			p_val = binom_test(num_sig, num_elts, p=prob)
			results.append((pair[0], group_names[i%len(group_names)], p_val))

	return results


def main(input_folder):
	data = get_san_input(input_folder)
	print("Group info:")
	results = bin_exact(data)
	print("\nResults:")
	for r in results:
		print(*r, sep="\t")
	

def process_arguments(args):
    '''Argparse function to get the program parameters'''
    parser = argparse.ArgumentParser(description='run_bin_exact.py')
    parser.add_argument('-i', '--input_folder',
                        action='store',
                        default="test.csv",
                        required=True,
                        help='name of folder to read data from')

    return vars(parser.parse_args(args))

if __name__ == "__main__":
	parameters = process_arguments(sys.argv[1:])
	main(**parameters)