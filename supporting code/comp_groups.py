import os
import argparse
import csv
import numpy
from itertools import zip_longest
import utilities as ut
import sys
from pvals import rand_pval
import itertools

def write_to_csv(data, csv_name):
	with open(csv_name, "w") as f:
		writer = csv.writer(f)
		writer.writerows(zip(*[[" "], ["Age"], ["Treatment"]]))
		writer.writerows(data)

def write_pairwise_to_csv(data, csv_name, group_names):
	with open(csv_name, "w") as f:
		writer = csv.writer(f)
		header = [[" "]]
		for comb in itertools.combinations([0, 1, 2, 3], 2):
			header.append([group_names[comb[0]] + " vs " + group_names[comb[1]]])
		
		writer.writerows(header)
		writer.writerows(zip(*data))

def comp(median, pairwise, verbose, group_names, file, item):
	if verbose:	
		print("-------------------------------------")
		print(item)
	raw_orig_data = numpy.genfromtxt(open(file, "rb"), skip_header=1, delimiter=",")

	# nuke nans
	new_cols = []
	for i in range(len(raw_orig_data[0])):
		new_cols.append([x for x in raw_orig_data[:, i] if not numpy.isnan(x)])
	
	# compare between each group
	pairwise_data = []
	if pairwise: 
		pairwise_data.append(os.path.splitext(item)[0])
		if verbose:
			print("Comparing groups!")
		for comb in itertools.combinations([0, 1, 2, 3], 2):
			comb_pval = rand_pval(new_cols[comb[0]], new_cols[comb[1]], use_median=median)

			if verbose:
				print(group_names[comb[0]], group_names[comb[1]], comb_pval)

			pairwise_data.append(str(comb_pval))

	data_to_plot = []
	data_to_plot.append(os.path.splitext(item)[0])

	# compare between ages
	adult = new_cols[0] + new_cols[1]
	p21 = new_cols[2] + new_cols[3]
	data_to_plot.append(str(rand_pval(adult, p21, use_median=median)))

	# compare between treatments
	pna = new_cols[0] + new_cols[2]
	veh = new_cols[1] + new_cols[3]
	data_to_plot.append(str(rand_pval(pna, veh, use_median=median)))	
	
	if verbose:
		print("Comparing ages!")
		print("P21 vs Adult", data_to_plot[1])
		print("Comparing treatments!")
		print("PNA vs VEH", data_to_plot[2])
		print("-------------------------------------")

	return data_to_plot, pairwise_data

def main(median, pairwise, verbose, output_file, input_folder, bw):
	group_names = ["Adult PNA", "Adult VEH", "P21 PNA", "P21 VEH"]

	# read in data
	# top_dir = "/Users/JP/work/mip_dev/cluster formatted folder"
	# top_dir = "/Users/JP/work/mip_dev/lg_formatted_bw_folders"
	top_dir = input_folder
	data = []
	pairwise_data = []
	
	# cluster / no extra nesting
	if not bw:
		for item in os.listdir(top_dir):
			file = os.path.join(top_dir, item)

			if os.path.isfile(file) and file.endswith(".csv"):
				group_d, pairwise_d = comp(median, pairwise, verbose, group_names, file, item)
				
				data.append(group_d)
				if pairwise:
					pairwise_data.append(pairwise_d)
	
	# vbw / extra folder nesting
	else:
		for f_item in os.listdir(top_dir):
			folder = os.path.join(top_dir, f_item)	

			if os.path.isdir(folder):
				if verbose:
					print("***********************************************")
				for item in os.listdir(folder):
					file = os.path.join(folder, item)

					if os.path.isfile(file) and file.endswith(".csv"):
						group_d, pairwise_d = comp(median, pairwise, verbose, group_names, file, item)

						data.append(group_d)
						if pairwise:
							pairwise_data.append(pairwise_d)

	# write_to_csv(data, "/Users/JP/work/mip_dev/new pval/median_cl_csv.csv")
	write_to_csv(data, output_file)
	
	if pairwise:
		write_pairwise_to_csv(pairwise_data, os.path.splitext(output_file)[0] + "pairwise.csv", group_names)

def process_arguments(args):
    '''Argparse function to get the program parameters'''
    parser = argparse.ArgumentParser(description='test_comp_cl_groups.py')
    parser.add_argument('-d', '--median',
                        action='store_true',
                        default=False,
                        required=False,
                        help='use diff of medians rather than diff of means')
    parser.add_argument('-p', '--pairwise',
                        action='store_true',
                        default=False,
                        required=False,
                        help='do pairwise between groups in addition to groups')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        default=False,
                        required=False,
                        help='prints out and makes csv, required for pairwise')
    parser.add_argument('-b', '--bw',
                        action='store_true',
                        default=False,
                        required=False,
                        help='read in vbw rather than cluster (more nested folders)')
    parser.add_argument('-o', '--output_file',
                        action='store',
                        default="test.csv",
                        required=True,
                        help='name of output file to write to')
    parser.add_argument('-i', '--input_folder',
                        action='store',
                        default="test.csv",
                        required=True,
                        help='name of folder to read data from')

    return vars(parser.parse_args(args))

if __name__ == "__main__":
	parameters = process_arguments(sys.argv[1:])
	main(**parameters)