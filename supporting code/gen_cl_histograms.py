import os
import array
import csv
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import re

def make_histogram(image_dir, file_name, data):
	plt.clf()

	if not os.path.exists(image_dir):
		os.makedirs(image_dir)

	# n, bins, patches = plt.hist(data, bins=range(0, 3000, 10), density=1, facecolor='blue', alpha=0.5)
	n, bins, patches = plt.hist(data, bins=range(int(min(data)), int(max(data)) + 2, 1), facecolor='blue', density=True, alpha=0.5)

	plt.xlabel('Number of Peaks')
	plt.ylabel('Frequency')
	plt.title('Num. Peaks for ' + file_name)

	# plt.savefig(os.path.join(image_dir, file_name + '.png'))
	plt.savefig(os.path.join(image_dir, file_name + '.pdf'))

	csv_name = os.path.join(image_dir, file_name + '.csv')
	csv_data = np.array(list(zip(bins, n)))
	np.savetxt(csv_name, csv_data, delimiter=',')

	return

top_dir = "/Users/JP/work/mip_dev/data/Cluster"
image_dir = "/Users/JP/work/mip_dev/rerun/data/cl histos"

for t_item in os.listdir(top_dir):
	# navigate through each group's folder
	group_dir = os.path.join(top_dir, t_item)
	if os.path.isdir(group_dir):
		print(t_item)

		# navigate through each cell folder
		for c_item in os.listdir(group_dir):
			cell_dir = os.path.join(group_dir, c_item)
			if os.path.isdir(cell_dir):
				# iterate through data for each bw
				for cl_item in os.listdir(cell_dir):
					cl_file = os.path.join(cell_dir, cl_item)

					if os.path.isfile(cl_file) and cl_file.endswith("or.txt"):
						# read in original data
						orig_data = np.loadtxt(open(cl_file, "rb"), skiprows=1)
						print(c_item, orig_data[2])
						
						# get corresponding mc file
						mc_file = cl_file.replace("or.txt", "cl.txt")
						mc_data = np.loadtxt(open(mc_file, "rb"), skiprows=1)
						hist_data = []
						for i in range(1000):
							hist_data.append(mc_data[i][2])
						filename = t_item + " " + c_item
						make_histogram(image_dir, filename, hist_data)

