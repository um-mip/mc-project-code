import numpy
import utilities as ut
from copy import deepcopy

class VBW_Cell:
	def __init__(self, name):
		self.name = name
		self.num_mc_bn_zeros = numpy.zeros(ut.MAX_BW_IDX)
		self.orig_data = numpy.zeros((ut.MAX_BW_IDX, ut.NUM_VBW_PARAMETERS))
		self.mc_data = numpy.zeros((ut.MAX_BW_IDX, ut.NUM_MC_RUNS, ut.NUM_VBW_PARAMETERS))

	def get_name(self):
		return self.name

	def get_mc_bn_zeros(self, bw_idx):
		return self.num_mc_bn_zeros[bw_idx]

	def get_bw_data(self, bw_idx, param):
		# return only non-nan things
		if param in (1, 2, 8):
			ret_mc_data = []
			for i in range(ut.NUM_MC_RUNS):
				if self.mc_data[bw_idx, i, 0]:
					ret_mc_data.append(self.mc_data[bw_idx, i, param])
			return self.orig_data[bw_idx][param], ret_mc_data
		else:
			return self.orig_data[bw_idx][param], self.mc_data[bw_idx, :, param]

	def add_orig_data(self, data, bw_idx):
		self.orig_data[bw_idx] = deepcopy(data)

	def add_mc_data(self, data, bw_idx):
		self.mc_data[bw_idx] = deepcopy(data)

		# determine and set num mc bn zeros
		n = 0
		for mc_run in range(ut.NUM_MC_RUNS):
			if self.mc_data[bw_idx][mc_run][0] == 0:
				n += 1
		self.num_mc_bn_zeros[bw_idx] = n

class Cluster_Cell:
	def __init__(self, name):
		self.name = name
		self.orig_data = numpy.zeros(ut.NUM_CLUSTER_PARAMETERS)
		self.mc_data = numpy.zeros((ut.NUM_MC_RUNS, ut.NUM_CLUSTER_PARAMETERS))

	def get_name(self):
		return self.name

	def get_data(self, param):
		return self.orig_data[param], self.mc_data[:, param]

	def add_orig_data(self, data):
		self.orig_data = deepcopy(data)

	def add_mc_data(self, data):
		self.mc_data = deepcopy(data)