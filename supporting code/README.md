# Research Project Supporting Code

This folder contains all of the code used for graph generation, general analysis outside of Igor, etc. 

NOTE: there are hardcoded filepaths scattered about that will need to be changed

# Files and their uses:

analyze_cluster.py/analyze_vbw.py: used to generate the <, >, <=, >=, = and cell vs itself resampling data

bn_vs_bw.py: used to generate data underlying Figure 3A

comp_groups.py: used to generate pairwise group resampling results

gen_bw_histograms.py/gen_cl_histograms.py: used to generate data underlying figures 1 and 5, respectively

gen_rand_pval_dist.py/get_param_ranges.py: used to generate data for use in validating the expected proportions for the binomial exact tests

gen_summary_data.py: used to generate data in Figure 4

make_interval_graph.py: used to generate data underlying Figure 3A

run_bin_exact.py: used to generate binomial exact test results

All other files just contain supporting functions, etc. used in the above files.