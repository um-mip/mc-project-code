import os
import argparse
import csv
import numpy
from itertools import zip_longest
import utilities as ut
import sys
from pvals import calc_perc, rand_pval
import math

def get_group_results(group, bw, param, func, use_rand_pval, median):
	group_data = []
	for k in range(group.get_num_cells()):
		# check on whether we should be plotting this (whether will be nans)
		num_bn_zeros = group.get_cell(k).get_mc_bn_zeros(bw)
		if num_bn_zeros == ut.NUM_MC_RUNS and param in (1, 2, 8):
			print("excluding", group.get_cell(k).get_name(), bw, ut.get_vbw_output_type(param))
			continue

		od, md = group.get_cell(k).get_bw_data(bw, param)

		if (math.isnan(od) or od == 0) and param in (1, 2, 8):
			print("excluding", group.get_cell(k).get_name(), bw, ut.get_vbw_output_type(param))
			continue

		pval = rand_pval([od], md, use_median=median) if use_rand_pval else calc_perc(od, md, ut.functors[func], ut.func_names[func])
		group_data.append(pval)
		if num_bn_zeros and param in (1, 2, 8):
			# pval = group.get_cell(k).get_p_val(param, bw, ut.functors[func], ut.func_names[func])
			print("warning", group.get_cell(k).get_name(), bw, ut.get_vbw_output_type(param), num_bn_zeros, pval)
		else:
			print(group.get_cell(k).get_name(), bw, ut.get_vbw_output_type(param), num_bn_zeros, pval)

	return group_data

def main(images, rand_pval, median, bws):
	# read in data
	top_dir = "/Users/JP/work/mip_dev/data/VBW"
	seen_groups, group_names = ut.read_vbw_data(top_dir)


	print("Starting generating images!")
	num_funcs_to_use = 1 if rand_pval else len(ut.functors)
	for func in range(num_funcs_to_use):
		if not rand_pval:
			print(ut.func_names[func])

		# create folder to house images
		img_dir = os.path.join("/Users/JP/work/mip_dev/rerun/data/cell_rerun", "full vbw " + ut.func_names[func] + " csvs")
		if not os.path.exists(img_dir):
			os.makedirs(img_dir)

		# start running through stuff to make images
		bws_to_use = [6, 21, 36, 51, 66, 81] if bws else range(ut.MAX_BW_IDX)
		for bw in bws_to_use: # create folder for burst window
			bw_dir = os.path.join(img_dir, "bw" + str(bw))
			if not os.path.exists(bw_dir):
				os.makedirs(bw_dir)	

			# run through each output type
			for param in range(ut.NUM_VBW_PARAMETERS):
				data_to_plot = []
				for group in seen_groups:
					data_to_plot.append(numpy.asarray(get_group_results(group, bw, param, func, rand_pval, median)))

				# gen images
				img_name = os.path.join(bw_dir, ut.get_vbw_output_type(param) + ".png")
				if images:
					ut.gen_cell_image(img_name, data_to_plot, group_names, "VBW", param, bw=bw)

				# save csvs
				csv_name = os.path.join(bw_dir, ut.get_vbw_output_type(param) + "_" + str(bw) + "_" + ut.func_names[func] + ".csv")
				ut.gen_cell_csv(csv_name, data_to_plot)

	print("Finished generating images!")

def process_arguments(args):
    '''Argparse function to get the program parameters'''
    parser = argparse.ArgumentParser(description='Voice assistant')
    parser.add_argument('-i', '--images',
                        action='store_true',
                        default=False,
                        required=False,
                        help='generate images for each param')
    parser.add_argument('-r', '--rand_pval',
                        action='store_true',
                        default=False,
                        required=False,
                        help='use the rand pval thing instead of <, >, =, <=, >=')
    parser.add_argument('-m', '--median',
                        action='store_true',
                        default=False,
                        required=False,
                        help='use median instead of mean, only useful when used with -r')
    parser.add_argument('-b', '--bws',
                        action='store_true',
                        default=False,
                        required=False,
                        help='use hardcoded bws instead of everything')

    
    return vars(parser.parse_args(args))

if __name__ == "__main__":
	parameters = process_arguments(sys.argv[1:])
	main(**parameters)