import utilities as ut
import numpy as np
from analyze_vbw import get_group_results
import csv
import os
import sys
import argparse
from itertools import zip_longest

def main(output_folder, median, verbose):
	if not os.path.exists(output_folder):
		os.makedirs(output_folder)

	# read in data
	top_dir = "/Users/JP/work/mip_dev/data/VBW"
	seen_groups, group_names = ut.read_vbw_data(top_dir)

	print("Starting generating images!")
	for func in range(len(ut.functors)):
		if verbose:
			print(ut.func_names[func])

		for param in range(ut.NUM_VBW_PARAMETERS):
			if verbose:
				print(ut.func_names[func], ut.get_vbw_output_type(param))

			data_to_plot = []
			legend_list = []

			# get avg for each group
			for group in seen_groups:
				legend_list.append([group.get_name()])
				avgs = []
				for bw in range(ut.MAX_BW_IDX):
					group_data = np.asarray(get_group_results(group, bw, param, func, False, median))

					if median:
						avgs.append(np.median(group_data))
					else:
						avgs.append(np.mean(group_data))

				data_to_plot.append(avgs)

			# write to csvs
			csv_name = os.path.join(output_folder, ut.get_vbw_output_type(param) + "_" + ut.func_names[func] + ".csv")
			with open(csv_name, "w") as f:
				writer = csv.writer(f)
				writer.writerows(zip(*legend_list))
				writer.writerows(zip_longest(*data_to_plot))

def process_arguments(args):
    '''Argparse function to get the program parameters'''
    parser = argparse.ArgumentParser(description='Generate Summary Images')
    parser.add_argument('-o', '--output_folder',
                        action='store',
                        default='test.csv',
                        required=True,
                        help='folder to store data to')
    parser.add_argument('-d', '--median',
                        action='store_true',
                        default=False,
                        required=False,
                        help='use medians rather than means')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        default=False,
                        required=False,
                        help='print status')
    
    return vars(parser.parse_args(args))

if __name__ == "__main__":
	parameters = process_arguments(sys.argv[1:])
	main(**parameters)