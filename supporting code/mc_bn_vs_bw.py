import os
import csv
import numpy
import pylab
import re
import numpy as numpy
import matplotlib as mpl
import matplotlib.pyplot as plt
import statistics
from itertools import zip_longest

def get_output_type(j):
	if j == 0: 
		return "bn"
	elif j == 1:
		return "mbd"
	elif j == 2:
		return "spb"
	elif j == 3:
		return "bf"
	elif j == 4:
		return "ssn"
	elif j == 5:
		return "ssf"
	elif j == 6:
		return "tf"
	elif j == 7:
		return "inter"
	elif j == 8:
		return "intra"
	else:
		return "godspeed"

class Cell:
	def __init__(self, name):
		self.name = name
		self.p_vals = numpy.zeros((201, 9))

	def get_name(self):
		return self.name

	def get_p_val(self, row, col):
		return self.p_vals[row][col]

	def add_bw_p_vals(self, bw, p_vals):
		self.p_vals[bw] = p_vals[:]

class Group:
	def __init__(self, name):
		self.name = name
		self.cells = []
		self.medians = numpy.zeros((201, 9))

	def get_name(self):
		return self.name

	def get_num_cells(self):
		return len(self.cells)

	def get_cell(self, i):
		return self.cells[i]

	def add_cell(self, cell_to_add):
		self.cells.append(cell_to_add)

	def add_median(self, bw_idx, out_param_idx, median):
		self.medians[bw_idx][out_param_idx] = median

	def get_medians(self):
		return self.medians

# read in data
seen_groups = []
group_names = []
top_dir = "/Users/JP/work/mip_dev/data/VBW"
print("Reading data from", top_dir, "!")

# top level folder
for t_item in os.listdir(top_dir):
	# navigate through each group's folder
	group_dir = os.path.join(top_dir, t_item)
	if os.path.isdir(group_dir):
		# decided is a group, create new one to reflect
		new_group = Group(t_item)
		print(t_item)

		# navigate through each cell folder
		for c_item in os.listdir(group_dir):
			cell_dir = os.path.join(group_dir, c_item)
			if os.path.isdir(cell_dir):
				# decided have new cell, create one to reflect
				new_cell = Cell(c_item)
				print(c_item)

				# iterate through data for each bw
				for bw_item in os.listdir(cell_dir):
					# p_vals = numpy.zeros((1, 9))
					bw_file = os.path.join(cell_dir, bw_item)

					if os.path.isfile(bw_file) and bw_file.endswith("mc.txt"):
						# get bw index from file name
						bw_idx = int(re.findall(r"(?<=_)\d+", bw_item)[0])

						# read in original data
						mc_data = numpy.loadtxt(open(bw_file, "rb"), skiprows=1)

						# temp to hold 
						p_vals = numpy.zeros((1, 9))
						
						# run through each output 
						for col in range(numpy.size(mc_data, 1)):
							
							# p_vals[0, col] = orig_data[col]

							p_vals[0, col] = 0
							for row in range(numpy.size(mc_data, 0)):
								p_vals[0, col] += mc_data[row][col]

							# find mean of the thing
							p_vals[0, col] /= numpy.size(mc_data, 0)

						# done with this bw, add to cell
						new_cell.add_bw_p_vals(bw_idx, p_vals)

				# done modifying this cell, add to group
				new_group.add_cell(new_cell)

		# done modifying group, add to list of groups
		seen_groups.append(new_group)
		group_names.append(new_group.get_name())

print("Finished reading data!")
print("Starting generating csvs!")

# create folder to house images
img_dir = os.path.join("/Users/JP/work/mip_dev/rerun/data/mc bn vs bw", "test mc vbw bn vs bw csvs")
if not os.path.exists(img_dir):
	os.makedirs(img_dir)

# one csv with each cell as a column for each group
for group in seen_groups:
	data_to_plot = []
	cell_names = []
	for i in range(group.get_num_cells()):
		bns = []
		for j in range(201):
			bns.append(group.get_cell(i).get_p_val(j, 0))
		data_to_plot.append(bns)
		cell_names.append([group.get_cell(i).get_name()])

	# save csv
	csv_name = os.path.join(img_dir, group.get_name() + ".csv")
	with open(csv_name, "w") as f:
		writer = csv.writer(f)
		writer.writerows(zip(*cell_names))
		writer.writerows(zip_longest(*data_to_plot))

# csv with cells from each group added at each burst window
group_sums = []
group_names = []
group_avgs = []
group_meds = []
for group in seen_groups:
	bn_sums = [0] * 201
	for j in range(group.get_num_cells()):
		for i in range(201):
			bn_sums[i] += group.get_cell(j).get_p_val(i, 0)
	group_sums.append(bn_sums)
	
	bn_avgs = [0] * 201
	for z in range(201):
		bn_avgs[z] = bn_sums[z] / group.get_num_cells()
	group_avgs.append(bn_avgs)

	# median calculation
	bn_meds = [0] * 201
	for k2 in range(201):
		group_data = []
		for k in range(group.get_num_cells()):
			group_data.append(group.get_cell(k).get_p_val(k2, 0))	
		group_data_arr = numpy.asarray(group_data)
		bn_meds[k2] = statistics.median(group_data_arr)
	group_meds.append(bn_meds)

	group_names.append([group.get_name()])

csv_name2 = os.path.join(img_dir, "groups summed.csv")
with open(csv_name2, "w") as d:
	writer = csv.writer(d)
	writer.writerows(zip(*group_names))
	writer.writerows(zip(*group_sums))

csv_name3 = os.path.join(img_dir, "groups avg.csv")
with open(csv_name3, "w") as s:
	writer = csv.writer(s)
	writer.writerows(zip(*group_names))
	writer.writerows(zip(*group_avgs))

csv_name4 = os.path.join(img_dir, "groups med.csv")
with open(csv_name4, "w") as s:
	writer = csv.writer(s)
	writer.writerows(zip(*group_names))
	writer.writerows(zip(*group_meds))

print("Finished generating csvs!")

# one csv with all cells summed up

# save csvs
# csv_name = os.path.join(bw_dir, get_output_type(j) + "_" + str(i) + "_" + ty + ".csv")
# with open(csv_name, "w") as f:
# 	writer = csv.writer(f)
# 	writer.writerows(zip(*[["Adult PNA"], ["Adult VEH"], ["P21 PNA"], ["P21 VEH"]]))
# 	writer.writerows(zip_longest(*data_to_plot))



