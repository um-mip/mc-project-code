import os
import argparse
import csv
import numpy
from itertools import zip_longest
import utilities as ut
import sys
from pvals import calc_perc, rand_pval

def get_group_results(group, param, func, use_rand_pval, median):
	group_data = []
	for k in range(group.get_num_cells()):

		od, md = group.get_cell(k).get_data(param)
		pval = rand_pval([od], md, use_median=median) if use_rand_pval else calc_perc(od, md, ut.functors[func], ut.func_names[func])
		group_data.append(pval)
		print(group.get_cell(k).get_name(), ut.get_cl_output_type(param), pval)

	return group_data

def main(images, rand_pval, median):
	# read in data
	top_dir = "/Users/JP/work/mip_dev/data/Cluster"
	seen_groups, group_names = ut.read_cl_data(top_dir)


	print("Starting generating images!")
	num_funcs_to_use = 1 if rand_pval else len(ut.functors)
	for func in range(num_funcs_to_use):
		if not rand_pval:
			print(ut.func_names[func])

		# create folder to house images
		img_dir = os.path.join("/Users/JP/work/mip_dev/rerun/data", "cl " + ut.func_names[func] + " rand median csvs")
		if not os.path.exists(img_dir):
			os.makedirs(img_dir)

		# run through each output type
		for param in range(ut.NUM_CLUSTER_PARAMETERS):
			data_to_plot = []
			for group in seen_groups:
				data_to_plot.append(numpy.asarray(get_group_results(group, param, func, rand_pval, median)))

			# gen images
			img_name = os.path.join(img_dir, ut.get_cl_output_type(param) + ".png")
			if images:
				ut.gen_cell_image(img_name, data_to_plot, group_names, "CL", param)

			# save csvs
			csv_name = os.path.join(img_dir, ut.get_cl_output_type(param) + "_" + ut.func_names[func] + ".csv")
			ut.gen_cell_csv(csv_name, data_to_plot)

	print("Finished generating images!")

def process_arguments(args):
    '''Argparse function to get the program parameters'''
    parser = argparse.ArgumentParser(description='Voice assistant')
    parser.add_argument('-i', '--images',
                        action='store_true',
                        default=False,
                        required=False,
                        help='generate images for each param')
    parser.add_argument('-r', '--rand_pval',
                        action='store_true',
                        default=False,
                        required=False,
                        help='use the rand pval thing instead of <, >, =, <=, >=')
    parser.add_argument('-m', '--median',
                        action='store_true',
                        default=False,
                        required=False,
                        help='use median instead of mean, only useful when used with -r')
    
    return vars(parser.parse_args(args))

if __name__ == "__main__":
	parameters = process_arguments(sys.argv[1:])
	main(**parameters)